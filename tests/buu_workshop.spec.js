import { test,expect } from '@playwright/test';
const settrade_web = 'https://www.settrade.com/th/home';

const list_qoute = ['PTT'];

test('search_quote_settrade', async ({ context }) => {
    const page = await context.newPage();
    //await page.pause();
    await page.goto(settrade_web);
    await page.locator('input[class="form-control shadow-none border-0 rounded-0 p-0"]').type(list_qoute[0]);
    
    if (await page.isVisible(`div[data-value="${list_qoute[0]}"]`)) {
        const [newPage] = await Promise.all([
            context.waitForEvent('page'),
            await page.locator(`span[class="text-highlight"]:has-text("${list_qoute[0]}")`).first().click()
        ])
        await newPage.waitForTimeout(2000);
        await expect(newPage.locator('h1[class="text-neutral-deep-gray mb-0 me-3"]')).toHaveText(list_qoute[0]);
        await newPage.locator('div[class="site-container pt-2 pt-lg-0 mb-lg-4"]').screenshot({ path: 'tests/BUU/search_FoundData.png'})
    } else {
        await expect(page.locator('text = ไม่พบชื่อย่อหลักทรัพย์ที่ค้นหา')).toHaveAttribute('class', 'title-font-family text-center my-3 text-middle-gray');
        await page.locator('div[class="quote-dropdown w-100 bg-white pb-2"]').screenshot({ path: 'tests/BUU/search_NotFoundData.png'})

    }
});